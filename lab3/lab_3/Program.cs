﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_3
{
    class Program
    {
        class Car
        {
            public string Name = "Toyota";           
            public event Action<string> Ev;

            public Car()
            {
                    
            }

            public Car(string name)
            {
                Name = name;
            }

            public void ChangeName(string name)
            {
                if (name != Name)
                {
                    if (Change())
                    {
                        Name = name;
                        Ev(name);
                    }
                    
                }
            }
        }


        static void Main(string[] args)
        {           
            Car car = new Car();
            car.Ev += CarNameChenged;

            Console.WriteLine($"Назва машини: {car.Name}");
            
            while (true)
            {                
                Console.Write("Введiть нову назву машини: ");
                string newName = Console.ReadLine();
                while (newName == car.Name)
                {
                    Console.WriteLine("Нове iм'я автомобiля не вiдрiзнається вiд поточного, введiть iнше i'мя.");
                    newName = Console.ReadLine();
                    continue;
                }
                car.ChangeName(newName);

                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();
            }
        }

        private static void CarNameChenged(string newName)
        {
            if (newName == null)
            {
                Console.WriteLine("\tНазву машини не змiнено");
                return;
            }
            Console.WriteLine($"\tНазву машини змiнено!      Нова назва машини: {newName}");
        }

        private static bool Change()
        {
            Console.WriteLine("Змiнити назву машини? (да - клавiша<Y>, нi - клавiша<N>)");            
            char key = Console.ReadKey(true).KeyChar;
            while(true)
            { 
                if (key == 'y' || key == 'Y')
                {
                    return true;
                } 

                if(key == 'n' || key == 'N')
                {
                    return false;
                }
                else
                {
                    Console.WriteLine("Некоректний ввiд. Змiнити назву машини? (да - клавiша<Y>, нi - клавiша<N>)");
                    key = Console.ReadKey(true).KeyChar;
                }                
            }
        }
    }
}
