﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3_2
{
    class Program
    {
        class Args : EventArgs
        {
            private int numOfRecever;
            public int NumOfRecever
            {
                get
                {
                    return numOfRecever;
                }
                set
                {
                    numOfRecever = value;
                }
            }
            private int info;
            public int Info
            {
                get
                {
                    return info;
                }
            }
            
           
            public Args(int numOfRecever, int info)
            {
                this.numOfRecever = numOfRecever;
                this.info = info;                
            }
        }
        class GenEvent 
        {
            
            public delegate void Event
                (Args e, object sender);
            

            static bool sendTooFirst = true;

            public event Event event1;
            public event Event event2;
            public event Event event3;
            
            public void Send(int numOfRecever, int info)
            {
                Args e = new Args(numOfRecever, info);

                if (sendTooFirst)
                {
                    sendTooFirst = false;
                    if (event1 == null)
                    {
                        this.Send(--numOfRecever, info);
                    }
                    else
                        event1.Invoke(e, this);
                }
                else

                switch (numOfRecever)
                {
                    case 0:
                        Console.WriteLine("Номер приймача: 0, завершую роботу...");
                        break;

                    case 1:
                        if (event1 == null)
                        {
                            this.Send(--numOfRecever, info);
                        }
                        else
                            event1.Invoke(e, this);
                        break;

                    case 2:
                        if (event2 == null)
                        {
                            this.Send(--numOfRecever, info);
                        }
                        else
                            event2.Invoke(e, this);
                        break;

                    case 3:
                        if (event3 == null)
                        {
                            this.Send(--numOfRecever, info);
                        }
                        else
                            event3.Invoke(e, this);
                        break;
                }
            }
            

        }
        

        class Rec1
        {
               
            void OnRecChange(Args e, object sender)
            {
                Console.WriteLine($"Отримувач 1: {e.Info}");
                ((GenEvent)sender).Send(--e.NumOfRecever, e.Info % 111);                
            }
            public Rec1(GenEvent gnEvent)
            {
                gnEvent.event1 += new GenEvent.Event(OnRecChange);
            }
        }
        
        class Rec2
        {
            void OnRecChange2(Args e, object sender)
            {
                Console.WriteLine($"Отримувач 2: {e.Info}");
                ((GenEvent)sender).Send(--e.NumOfRecever, e.Info % 333);
            }
            public Rec2(GenEvent gnEvent2)
            {
                gnEvent2.event2 += new GenEvent.Event(OnRecChange2);
            }

        }
        
        class Rec3
        {
           
            void OnRecChange3(Args e, object sender)
            {
                Console.WriteLine($"Отримувач 3: {e.Info}");
                ((GenEvent)sender).Send(--e.NumOfRecever, e.Info % 999);                
            }

            
            public Rec3(GenEvent gnEvent3)
            {
                gnEvent3.event3 += new GenEvent.Event(OnRecChange3);
            }

        }
        class Class1
        {
            static void Main(string[] args)
            {
               

                GenEvent gnEvent = new GenEvent();
                Rec1 inventoryWatch = new Rec1(gnEvent);
                Rec2 inventoryWatch2 = new Rec2(gnEvent);
                Rec3 inventoryWatch3 = new Rec3(gnEvent);

                gnEvent.Send(3, 54572);
                Console.WriteLine();
                Console.WriteLine();
                gnEvent.Send(2, 40001);
                Console.WriteLine();
                Console.WriteLine();
                gnEvent.Send(3, 12345);

                Console.ReadLine();
            }
        }
    }
}
