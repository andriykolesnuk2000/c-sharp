﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Threading;

namespace lab4_2
{
    public class Proc
    {
        
        public string Name { get; set; }
        public long Memory { get; set; }
        public DateTime Time { get; set; }
        public string Priority { get; set; }
        public int CountThreads { get; set; }
        public int Id { get; set; }

        public Proc()
        {
        }

        public Proc(string name, long memory, DateTime time, string priority, int countThreads, int id)
        {
            Name = name;
            Memory = memory;
            Time = time;
            Priority = priority;
            CountThreads = countThreads;
            Id = id;
        }        
    }

    public class Processes
    {
        public List<Proc> processes = new List<Proc>();

        public Processes()
        {

            foreach (var proc in Process.GetProcesses())
                try
                {
                    processes.Add(new Proc(proc.ProcessName, proc.VirtualMemorySize64, proc.StartTime, proc.PriorityClass.ToString(), proc.Threads.Count, proc.Id));
                }
                catch { }
        }

        public void Upadate()
        {
            Process curProcess;

            for (int i = 0; i < processes.Count; i++)
            {
                Proc item = processes[i];
                try
                {
                    curProcess = Process.GetProcessById(item.Id);

                    processes[i].Memory = curProcess.VirtualMemorySize64;
                    processes[i].CountThreads = curProcess.Threads.Count;
                    processes[i].Priority = curProcess.PriorityClass.ToString();
                }
                catch
                {
                    processes.Remove(item);
                }

            }
        }

        public void FindNewProcesses()
        {
            foreach(Process proc in Process.GetProcesses())
            {
                if (processes.Exists(p => p.Id == proc.Id))
                    continue;
                else
                {
                    try
                    {
                        processes.Add(new Proc(proc.ProcessName, proc.VirtualMemorySize64, proc.StartTime, proc.PriorityClass.ToString(), proc.Threads.Count, proc.Id));
                    }
                    catch { }
                }                
            }
        }
    }




    public partial class MainWindow : Window
    {
        private static Processes processes = new Processes();
        private static DataGrid dataGrid;


        public MainWindow()
        {
            this.Top = 20;

            InitializeComponent();

            ProcessesGrid.ItemsSource = processes.processes;

            dataGrid = ProcessesGrid;
        }

        private Timer timer = new Timer(UpdateProcesses, dataGrid, 2000, 2000);
        public static void UpdateProcesses(object obj)
        {
            processes.Upadate();
            processes.FindNewProcesses();
            dataGrid.Dispatcher.Invoke((Action)(() =>
            {
                dataGrid.Items.Refresh();
                dataGrid.Focus();
            }
            )) ;
            
        }

        private void Remove(object sender, RoutedEventArgs e)
        {   
            try
            {
                Proc selected = ProcessesGrid.SelectedItem as Proc;
                Process.GetProcessById(selected.Id).Kill();
                MessageBox.Show("Процес завершено.");
                processes.processes.Remove(selected);
                return;
            }
            catch
            {
                MessageBox.Show("Помилка при завершенні процеса.");
                return;
            }

        }

        private void Change(object sender, RoutedEventArgs e)
        {
            try
            {
                Proc selected = ProcessesGrid.SelectedItem as Proc; 
                ChangePriority changer = new ChangePriority(Process.GetProcessById(selected.Id));
                changer.Show();
            }
            catch
            {
                MessageBox.Show("Помилка при зміні пріорітету.");                
            }
        }

        private void Kalculator(object sender, RoutedEventArgs e)
        {
            Process.Start("calc.exe");
        }

        private void MicrosoftWord(object sender, RoutedEventArgs e)
        {
            Process.Start("wordpad.exe");
        }

        private void Paint(object sender, RoutedEventArgs e)
        {
            Process.Start("mspaint.exe");
        }

        private void Chrome(object sender, RoutedEventArgs e)
        {
            Process.Start("chrome.exe");
        }

        private void Steam(object sender, RoutedEventArgs e)
        {
            Process.Start("Steam.exe");
        }
    }
}
