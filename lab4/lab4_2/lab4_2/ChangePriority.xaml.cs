﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;

namespace lab4_2
{
    /// <summary>
    /// Логика взаимодействия для ChangePriority.xaml
    /// </summary>
    public partial class ChangePriority : Window
    {
        private Process selectedProcess;

        public ChangePriority(Process Process)
        {
            InitializeComponent();

            

            SetProcess(Process);
        }

        private void SetProcess(Process Process)
        {
            if (Process != null)
                selectedProcess = Process;
            else
                this.Close();
        }

        private void BelowNormal(object sender, RoutedEventArgs e)
        {
            try
            {
                selectedProcess.PriorityClass = ProcessPriorityClass.BelowNormal;
                this.Close();
                MessageBox.Show("Приорітет успішно змінено");
            }
            catch
            {
                this.Close();
                MessageBox.Show("Помилка при зміні приорітету");
            }
            
        }

        private void Normal(object sender, RoutedEventArgs e)
        {
            try
            {
                selectedProcess.PriorityClass = ProcessPriorityClass.Normal;
                this.Close();
                MessageBox.Show("Приорітет успішно змінено");
            }
            catch
            {
                this.Close();
                MessageBox.Show("Помилка при зміні приорітету");
            }
        }

        private void AboveNormal(object sender, RoutedEventArgs e)
        {
            try
            {
                selectedProcess.PriorityClass = ProcessPriorityClass.AboveNormal;
                this.Close();
                MessageBox.Show("Приорітет успішно змінено");
            }
            catch
            {
                this.Close();
                MessageBox.Show("Помилка при зміні приорітету");
            }
        }

        private void Hight(object sender, RoutedEventArgs e)
        {
            try
            {
                selectedProcess.PriorityClass = ProcessPriorityClass.High;
                this.Close();
                MessageBox.Show("Приорітет успішно змінено");
            }
            catch
            {
                this.Close();
                MessageBox.Show("Помилка при зміні приорітету");
            }
        }
    }
}
