﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4
{
    public class Args
    {
        public OpenFileDialog Ofl { get; set; }

        public string Key { get; set; }

        public string Vector { get; set; }

        public Args(OpenFileDialog ofl, string key, string vector)
        {
            Ofl = ofl;
            Key = key;
            Vector = vector;
        }
    }
}
