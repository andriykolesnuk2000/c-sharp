﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace lab4
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static double time = 0;
        static Timer timer;

        byte[] defoultKey = { 0, 1, 2, 3, 4, 5, 6, 7 };
        byte[] defoultVector = { 0, 1, 2, 3, 4, 5, 6, 7 };

        public BackgroundWorker BWEncrypt = new BackgroundWorker();
        public BackgroundWorker BWDecrypt = new BackgroundWorker();

        //public static TimerCallback callback = new TimerCallback();

        public void changeLabel1(object obj)
        {            
            time = time + 0.1;
            Dispatcher.Invoke((Action)(() => label1.Content = time.ToString("0.0") + " c."));            
        }

        public MainWindow()
        {
            InitializeComponent();

            InitializeBackgroundWorkers();
        }

        private void InitializeBackgroundWorkers()
        {
            BWEncrypt.DoWork += new DoWorkEventHandler(Encrypting);
            BWEncrypt.ProgressChanged += new ProgressChangedEventHandler(ProgressChanged);
            BWEncrypt.WorkerReportsProgress = true;

            BWDecrypt.DoWork += new DoWorkEventHandler(Decrypting);
            BWDecrypt.ProgressChanged += new ProgressChangedEventHandler(ProgressChanged);
            BWDecrypt.WorkerReportsProgress = true;
        }

        private byte[] GetKey(string key)
        {
            byte[] resultKey = new byte[8];

            if (key == "")
                return new byte[] { 0, 1, 2, 3, 4, 5, 6, 7 };
            else
            {
                for (int i = 0; i < 8; i++)
                {
                    resultKey[i] = byte.Parse(key[i % key.Length].ToString());

                }
                return resultKey;
            }            
        }

        private void ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.Dispatcher.Invoke((Action)(() => Progress.Value = e.ProgressPercentage));
        }

        private void Decrypting(object sender, DoWorkEventArgs args)
        {
            Args decArgs = args.Argument as Args;

            OpenFileDialog ofl = decArgs.Ofl;

            this.Dispatcher.Invoke((Action)(() =>
            {
                time = 0;
                timer = new Timer(changeLabel1, null, 0, 100);
            }));

            string inName = ofl.FileName;
            string outName = ofl.FileName.Replace(ofl.SafeFileName, "dencrypted.bin");


            FileStream fin = new FileStream(inName, FileMode.Open, FileAccess.Read);
            FileStream fout = new FileStream(outName, FileMode.OpenOrCreate, FileAccess.Write);
            fout.SetLength(0);


            byte[] bin = new byte[1000];
            long rdlen = 0;
            long totlen = fin.Length;


            DES des = new DESCryptoServiceProvider();

            
            des.Key = GetKey(decArgs.Key);
            des.IV = GetKey(decArgs.Vector);
            

            CryptoStream encStream = new CryptoStream(fout, des.CreateDecryptor(), CryptoStreamMode.Write);

            long onePercent = (totlen / 1000) / 100;
            int countI = 0;
            int progress = 0;

            while (rdlen < totlen)
            {
                countI++;

                int len = fin.Read(bin, 0, 1000);
                encStream.Write(bin, 0, len);
                rdlen = rdlen + len;

                if (countI == onePercent)
                {
                    progress++;
                    (sender as BackgroundWorker).ReportProgress(progress);
                    //this.Dispatcher.Invoke((Action)(() => Progress.Value += 1));
                    countI = 0;
                }
            }


            encStream.Close();
            fout.Close();
            fin.Close();


            this.Dispatcher.Invoke((Action)(() =>
            {
                timer.Dispose();
                label1.Content = "";
                //Progress.Value = 0;
            }));

            MessageBox.Show("Iм'я розшифрованого файлу: " + ofl.FileName +
                            "\nЧас витрачений на дешифрування: " + time.ToString() + " c." +
                            "\nРозмiр файлу: " + totlen.ToString("000 000 000") + " байт");

            ((BackgroundWorker)sender).ReportProgress(0);

        }

        private void Encrypting(object sender, DoWorkEventArgs args)
        {
            Args encArgs = args.Argument as Args;

            OpenFileDialog ofl = encArgs.Ofl;

            this.Dispatcher.Invoke((Action)(() =>
            {
                time = 0;
                timer = new Timer(changeLabel1, null, 0, 100);
            }));

            string inName = ofl.FileName;
            string outName = ofl.FileName.Replace(ofl.SafeFileName, "encrypted.bin");


            FileStream fin = new FileStream(inName, FileMode.Open, FileAccess.Read);
            FileStream fout = new FileStream(outName, FileMode.OpenOrCreate, FileAccess.Write);
            fout.SetLength(0);


            byte[] bin = new byte[1000];
            long rdlen = 0;
            long totlen = fin.Length;
            

            DES des = new DESCryptoServiceProvider();

            des.Key = GetKey(encArgs.Key);
            des.IV = GetKey(encArgs.Vector);

            CryptoStream encStream = new CryptoStream(fout, des.CreateEncryptor(), CryptoStreamMode.Write);

            long onePercent = (totlen / 1000) / 100;
            int countI = 0;
            int progress = 0;

            while (rdlen < totlen)
            {
                countI++;

                int len = fin.Read(bin, 0, 1000);
                encStream.Write(bin, 0, len);
                rdlen = rdlen + len;

                if (countI == onePercent)
                {
                    progress++;
                    (sender as BackgroundWorker).ReportProgress(progress);
                    //this.Dispatcher.Invoke((Action)(() => Progress.Value += 1));
                    countI = 0;
                }
            }


            encStream.Close();
            fout.Close();
            fin.Close();


            this.Dispatcher.Invoke((Action)(() =>
            {
                timer.Dispose();
                label1.Content = "";
                //Progress.Value = 0;
            }));

            MessageBox.Show("Iм'я зашифрованого файлу: " + ofl.FileName +
                            "\nЧас витрачений на шифрування: " + time.ToString() + " c." +
                            "\nРозмiр файлу: " + totlen.ToString("000 000 000") + " байт");

            ((BackgroundWorker)sender).ReportProgress(0);

        }

        private void EncryptFile(object sender, RoutedEventArgs e)
        {
            
            OpenFileDialog ofl = new OpenFileDialog();

            ofl.ShowDialog();
            if (ofl.FileName != null)
            {
                
                
                Args args = new Args(ofl, keyText.Text, vectorText.Text);

                BWEncrypt.RunWorkerAsync(args);
                //BW.DoWork -= new DoWorkEventHandler(Encripting);
            }
            


            //Encripting(ofl.FileName, null);

            //    textBox1.Text = ofl.FileName;
            //ofl.Dispose();
        }

        private void DecryptFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofl = new OpenFileDialog();

            ofl.ShowDialog();
            if (ofl.FileName != null)
            {
                Args args = new Args(ofl, keyText.Text, vectorText.Text);

                BWDecrypt.RunWorkerAsync(args);                
            }
        }
    }
}
