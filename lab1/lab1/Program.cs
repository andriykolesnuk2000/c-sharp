﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;



namespace lab1
{
    [System.Runtime.InteropServices.Guid("A6BFDD17-A395-415A-9C5D-7857B9234816")]
    class Matrix
    {
        public int height { get; private set; }
        public int width { get; private set; }
        private int[,] mas;

        public Matrix(int a, int b)
        {
            height = a;
            width = b;
            mas = new int[a, b];

        }

        public Matrix()
        {
        }

        public int this[int index1, int index2]
        {
            get
            {
                return mas[index1, index2];
            }

            set
            {
                this.mas[index1, index2] = value;
            }
        }


        public void Rand()
        {
            Random rand = new Random((int)DateTime.Now.Ticks);
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {

                    mas[i, j] = rand.Next(-10, 10);

                }
            }
        }

        public void Print()
        {
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    Console.Write($"{mas[i, j]} \t");
                }
                Console.WriteLine();
            }
        }

        public Matrix Transport()
        {
            Matrix result = new Matrix(this.width, this.height);
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    result[j, i] = this[i, j];
                }
            }
            return result;
        }

        public int Determinant()
        {
            if (mas.Length == 1)
            {
                return mas[0, 0];
            }

            if (mas.Length == 4)
            {
                return mas[0, 0] * mas[1, 1] - mas[0, 1] * mas[1, 0];
            }

            int sign = 1, result = 0;

            for (int i = 0; i < width; i++)
            {
                int[,] minor = GetMinor(mas, i);
                result += sign * mas[0, i] * Determinant(minor);
                sign = -sign;
            }
            return result;
        }

       public static int Determinant(int[,] mas)
        {
            if (mas.Length == 1)
            {
                return mas[0, 0];
            }

            if (mas.Length == 4)
            {
                return mas[0, 0] * mas[1, 1] - mas[0, 1] * mas[1, 0];
            }
            int sign = 1, result = 0;
            for (int i = 0; i < mas.GetLength(1); i++)
            {
                int[,] minor = GetMinor(mas, i);
                result += sign * mas[0, i] * Determinant(minor);
                sign = -sign;
            }
            return result;
        }


        private static int[,] GetMinor(int[,] mas, int n)
        {
            int[,] result = new int[mas.GetLength(0) - 1, mas.GetLength(0) - 1];
            for (int i = 1; i < mas.GetLength(0); i++)
            {
                for (int j = 0, col = 0; j < mas.GetLength(1); j++)
                {
                    if (j == n)
                        continue;
                    result[i - 1, col] = mas[i, j];
                    col++;
                }
            }
            return result;
        }


        public static Matrix operator *(Matrix m1, Matrix m2)
        {
            if (m1.width != m2.height)
            {
                return null;
            }

            Matrix result = new Matrix(m1.height, m2.width);

            for (int i = 0; i < m1.height; i++)
            {
                for (int j = 0; j < m2.width; j++)
                {
                    for (int k = 0; k < m2.height; k++)
                    {
                        result[i, j] += m1[i, k] * m2[k, j];
                    }
                }
            }

            return result;
        }
                       

        public double[,] Inverse()
        {
            if(height != width || this.Determinant() == 0)
            {
                Console.WriteLine("Обернена матрицяя не існує");
                return null;
            }
            

            int n = height;
            double[,] a = new double[n,n];

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    a[i, j] = mas[i, j];
                }
            }

            double[,] ans = new double[n,n];
            for (int i = 0; i < n; i++)
            {
                ans[i,i] = 1;
            }
            for (int i = 0; i < n; i++)
            {
                int row = i;
                double mx = a[i,i];
                for (int k = i + 1; k < n; k++)
                {
                    if (Math.Abs(a[k,i]) > mx)
                    {
                        row = k;
                        mx = Math.Abs(a[k,i]);
                    }
                }
                if (row != i)
                {
                    for (int j = 0; j < a.GetLength(0); j++)
                    {
                        double tmp = a[row, j];
                        a[row, j] = a[i, j];
                        a[i, j] = tmp;
                    }

                    for (int j = 0; j < ans.GetLength(0); j++)
                    {
                        double tmp = ans[row, j];
                        ans[row, j] = ans[i, j];
                        ans[i, j] = tmp;
                    }

                }
                for (int j = i + 1; j < n; j++)
                {
                    double e = a[j,i] / a[i,i];
                    for (int k = 0; k < n; k++)
                    {
                        a[j,k] -= e * a[i,k];
                        ans[j,k] -= e * ans[i,k];
                    }
                }
            }
            for (int i = n - 1; i >= 0; i--)
            {
                for (int j = i - 1; j >= 0; j--)
                {
                    double e = a[j,i] / a[i,i];
                    for (int k = 0; k < n; k++)
                    {
                        a[j,k] -= e * a[i,k];
                        ans[j,k] -= e * ans[i,k];
                    }
                }
                for (int j = 0; j < n; j++)
                {
                    ans[i,j] /= a[i,i];
                }
            }

            for (int i = 0; i < ans.GetLength(0); i++)
            {
                for (int j = 0; j < ans.GetLength(1); j++)
                {
                    Console.Write($"{Math.Round(ans[i, j], 2)} \t");
                }
                Console.WriteLine();
            }

            return ans;
        }

        

    }

    
    class Program
    {
        

        static void Main(string[] args)
        {
           

            Matrix a = new Matrix(5, 3);
            Matrix b = new Matrix(3, 5);

            a.Rand();
            b.Rand();

            Console.WriteLine("Матриця А");
            a.Print();

            Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine("Матриця В");
            b.Print();

            Console.WriteLine();
            Console.WriteLine();

            Matrix c = a * b;

            Console.WriteLine("Матриця С = А * В");
            c.Print();

            Console.WriteLine();
            Console.WriteLine();

            c = c.Transport();

            Console.WriteLine("Транспонована матриця С");
            c.Print();

            Console.WriteLine();


            Console.WriteLine($"Визначник матрицi С = {c.Determinant()}");

            Console.WriteLine();

            Matrix d = new Matrix(2, 2);
            d[0, 0] = 3;
            d[0, 1] = 4;
            d[1, 0] = 5;
            d[1, 1] = 7;

            Console.WriteLine("Матриця Д");
            d.Print();

            Console.WriteLine();            

            Console.WriteLine($"Визначник матрицi Д = {d.Determinant()}");

            Console.WriteLine();            

            Console.WriteLine("Iнвертована матриця Д");
            d.Inverse();
            

            Console.ReadKey(true);

        }
    }
}
