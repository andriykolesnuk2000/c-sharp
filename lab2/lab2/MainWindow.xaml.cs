﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace lab2
{

    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    /// 

    public class Books
    {
        public static int count = 0;
        //public Book[] books = new Book[9];
        private List<Book> books = new List<Book>();
        
        [Serializable]
        public class Book
        {            
            public string Name;
            public string Author;
            public string Edition;

            public Book(string name, string author, string edition)
            {
                Name = name;
                Author = author;
                Edition = edition;
            }
            public Book()
            {              
            }
        }

        public void Add(string name, string author, string edition)
        {            
            books.Add(new Book(name, author, edition));            
            //books[count] = new Book(name, author, edition);
            count++;
        }

        public void Add(Book book)
        {
            books.Add(new Book(book.Name, book.Author, book.Edition));
            //books[count] = new Book(book.Name, book.Author, book.Edition);
            count++;
        }

        public void Add(Book[] book)
        {
            books.AddRange(book);           
            count += book.Length;
        }

        public void Sort(Func<Book, Book,bool> del)
        {
            bool needChange;

            for (int i = 0; i < count - 1; i++)
            {
                for (int j = 0; j < count - (1 + i); j++)
                {
                    needChange = del(books[j], books[j + 1]);
                    if(needChange)
                    {
                        Book tmp = books[j];
                        books[j] = books[j + 1];
                        books[j + 1] = tmp;
                    }
                }
            }
        }

        public void Cout()
        {
            MainWindow mainWindow = Application.Current.Windows[0] as MainWindow;            
            mainWindow.textBox1.Text = "";
            int len = 21;
            for (int i = 0; i < count; i++)
            {
                string name = books[i].Name;
                string author = books[i].Author;
                string edition = books[i].Edition;

                if(name.Length < len)
                    for (int j = 0; j < len - name.Length; j++)
                        name = name + " ";

                if (author.Length < len)
                    for (int j = 0; j < len - author.Length; j++)
                        author = author + " ";
                mainWindow.textBox1.Text += $"{name}{author}{edition}\n";
            }
        }
    }

    

    public partial class MainWindow : Window
    {
        public static bool SortByName(Books.Book b1, Books.Book b2)
        {
            int length;

            if (b1.Name.Length < b2.Name.Length)
                length = b1.Name.Length;
            else length = b2.Name.Length;

            for (int i = 0; i < length; i++)
            {
                if (b1.Name[i] > b2.Name[i])
                    return true;
                if (b1.Name[i] < b2.Name[i])
                    return false;
            }

            if (b1.Name.Length < b2.Name.Length)
                return false;
            else return true;
        }

        public static bool SortByAuthor(Books.Book b1, Books.Book b2)
        {
            int length;

            if (b1.Author.Length < b2.Author.Length)
                length = b1.Author.Length;
            else length = b2.Author.Length;

            for (int i = 0; i < length; i++)
            {
                if (b1.Author[i] > b2.Author[i])
                    return true;
                if (b1.Author[i] < b2.Author[i])
                    return false;
            }

            if (b1.Author.Length < b2.Author.Length)
                return false;
            else return true;
        }

        public static bool SortByEdition(Books.Book b1, Books.Book b2)
        {
            int length;

            if (b1.Edition.Length < b2.Edition.Length)
                length = b1.Edition.Length;
            else length = b2.Edition.Length;

            for (int i = 0; i < length; i++)
            {
                if (b1.Edition[i] > b2.Edition[i])
                    return true;
                if (b1.Edition[i] < b2.Edition[i])
                    return false;
            }

            if (b1.Edition.Length < b2.Edition.Length)
                return false;
            else return true;
        }

        Books b = new Books();
        public MainWindow()
        {
            InitializeComponent();

            /*Books.Book book = new Books.Book("a", "b", "c");
            Books.Book book1 = new Books.Book("a", "b", "c");
            Books.Book[] book2 = {book, book1};

            XmlSerializer serializer = new XmlSerializer(typeof(Books.Book[]));
            //books.Add("a", "b", "c");
            using (FileStream fs = new FileStream("books.xml", FileMode.OpenOrCreate))
            {
                serializer.Serialize(fs, book2);
            }*/            

            XmlSerializer serializer = new XmlSerializer(typeof(Books.Book[]));
            using (FileStream fs = new FileStream("books.xml", FileMode.OpenOrCreate))
            {
                Books.Book[] books = (Books.Book[])serializer.Deserialize(fs);
                foreach(var book in books)
                {
                    b.Add(book);
                }
            }
            b.Cout();
        }

        public static double time;
        static Timer timer;

        public delegate void calc();
        public delegate void SetTextBox(string text);

        TimerCallback tm = new TimerCallback(CalculateTime);

        public static void CalculateTime(object obj)
        {
            time += 0.01;
        }

        public void Calculate()
        {
            this.Dispatcher.Invoke((Action)(() => timer = new Timer(tm, null, 0, 10)));
            double result = 0;
            for (double i = -60000; i <= 10000; i += 0.025)
            {
                if (Math.Round(i, 3) % 70 == 0)
                    this.Dispatcher.Invoke((Action)(() => ProgressCalculate.Value += 0.1));
                double y = 15 * i / Math.Sqrt(5 * i + 1);
                result += y;
            }
            this.Dispatcher.Invoke(new SetTextBox((s) => TextBoxForResult.Text = s), result.ToString());
            this.Dispatcher.Invoke((Action)(() => {
                BeginCalculateButton.IsEnabled = true;
                ProgressCalculate.Value = 0;
                InfoLabel.Content = "";
                TimeTextBox.Text = Math.Round(time, 2).ToString();
                time = 0;
                timer.Dispose();
            }));            
            //TextBoxForResult.Text = result.ToString();            
        }
       
        public async Task CalculateAsync()
        {
            await Task.Run(() => Calculate());            
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            BeginCalculateButton.IsEnabled = false;
            InfoLabel.Content = "Процес обрахунку...";

            CalculateAsync();
            //calc del = new calc(Calculate);
            //async = del.BeginInvoke(null, null);
            
            //double result = del.EndInvoke(async);
            //TextBoxForResult.Text = result.ToString();
        }

        private void Label_AccessKeyPressed(object sender, AccessKeyPressedEventArgs e)
        {

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            b.Sort(SortByName);
            b.Cout();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            b.Sort(SortByAuthor);
            b.Cout();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            b.Sort(SortByEdition);
            b.Cout();
        }
    }
 }

