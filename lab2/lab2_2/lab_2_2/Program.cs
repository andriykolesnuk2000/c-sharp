﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_2_2
{
    public class First
    {
        public static int Function_1(int a)
        {
            Console.WriteLine("Function_1");
            return a * a * 3;
        }

        public int Function_2(int a)
        {
            Console.WriteLine("Function_2");
            return a * a + 4;
        }
    }

    class Second
    {
        int var = 5;
        public int Function_3(int a)
        {
            Console.WriteLine("Function_3");
            return Math.Abs(a) - var;
        }
    }
    class Program
    {
        public delegate int del(int a);     
		
        static void Main(string[] args)
        {
            First a = new First();
            Second b = new Second(); 

            del f = new del(First.Function_1);

            Func<int, int> f_2 = new Func<int, int>(First.Function_1);
            f_2 = f_2 + a.Function_2 + b.Function_3;

            Console.WriteLine("Виклик першого делегата");
            int result = f(10);

            Console.WriteLine("Виклик другого делегата");
            result = f_2(10);

            Console.ReadLine();
        }
    }
}
