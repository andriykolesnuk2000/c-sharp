﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Discovery;
using System.Diagnostics;
using Library;
using System.Drawing;

namespace Client
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        INet service;
        List<string> servers = new List<string>();
        string curAddress = "";

        public MainWindow()
        {
            InitializeComponent();

            UdpDiscoveryEndpoint endpoint = new UdpDiscoveryEndpoint();
            
            DiscoveryClient dc = new DiscoveryClient(endpoint);
            FindCriteria criteria = new FindCriteria(typeof(INet));
            criteria.Duration = new TimeSpan(0, 0, 1);
            FindResponse response = dc.Find(criteria);
            
            
            for (int i = 0; i < response.Endpoints.Count; i++)
            {
                servers.Add(response.Endpoints[i].Address.ToString());
            }

            lbServers.ItemsSource = servers;            
            lbServers.Items.Refresh();
        }

        private void InitializeService()
        {
            if (lbServers.SelectedItem != null && curAddress != lbServers.SelectedItem.ToString())
            {
                try
                {
                    curAddress = lbServers.SelectedItem.ToString();
                    BasicHttpBinding binding = new BasicHttpBinding();
                    binding.MaxReceivedMessageSize = 5_000_000;
                    ChannelFactory<INet> factory = new ChannelFactory<INet>(binding, curAddress);
                    // Открываем канал для общения клиента с со службой
                    service = factory.CreateChannel();
                }
                catch
                {
                    MessageBox.Show("Не удалось подключится к серверу");
                }
            }
        }

        private void SendMessageButton_Click(object sender, RoutedEventArgs e)
        {
            InitializeService();           
            
            try
            {               
                service.ShowMessage(MessageTextBox.Text);
            }
            catch
            {
                MessageBox.Show("Не вдалось отправить сообщение серверу");
            }

            
        }

        private void bUpdateServers_Click(object sender, RoutedEventArgs e)
        {
            UdpDiscoveryEndpoint endpoint = new UdpDiscoveryEndpoint();
            DiscoveryClient dc = new DiscoveryClient(endpoint);
            FindCriteria criteria = new FindCriteria(typeof(INet));
            criteria.Duration = new TimeSpan(0, 0, 1);
            FindResponse response = dc.Find(criteria);

            servers.Clear();
            for (int i = 0; i < response.Endpoints.Count; i++)
                servers.Add(response.Endpoints[i].Address.ToString());

            lbServers.ItemsSource = servers;
            lbServers.Items.Refresh();
        }

        private void bGetProcesses_Click(object sender, RoutedEventArgs e)
        {
            InitializeService();

            try
            {
                List<Proc> processes = service.GetProcesses();
                ProcessesWindow processesW = new ProcessesWindow(processes, service);
                processesW.Show();
            }
            catch
            {
                MessageBox.Show("Не вдалось отримати запущені процеси");
            }
        }

        private void bStartProgram_Click(object sender, RoutedEventArgs e)
        {
            InitializeService();
            try
            {
                service.StartProgram(tbProgramName.Text);
            }
            catch
            {
                MessageBox.Show("Не вдалось запустити програму");
            }
        }

        private void bGetPrintscreen_Click(object sender, RoutedEventArgs e)
        {
            InitializeService();
            try
            {   
                printscreenW printscreen = new printscreenW(service.GetScreen());
                printscreen.Show();
            }
            catch
            {
                MessageBox.Show("Не удалось получить скриншот екрана. Возможно вы не " +
                                "выбрали сервер для подключения или выбраный сервер не работает");
            }
        }
    }
}
