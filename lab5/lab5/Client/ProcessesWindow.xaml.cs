﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Library;

namespace Client
{
    /// <summary>
    /// Логика взаимодействия для ProcessesWindow.xaml
    /// </summary>
    public partial class ProcessesWindow : Window
    {
        INet service;
        List<Proc> processes;
        public ProcessesWindow(List<Proc> _processes, INet _service)
        {
            InitializeComponent();

            service = _service;
            processes = _processes;
            ProcessesGrid.ItemsSource = processes;
        }

        private void bKillProcess_Click(object sender, RoutedEventArgs e)
        {
            Proc selected = ProcessesGrid.SelectedItem as Proc;
            if (service.KillProcessById(selected.Id))
            {
                processes.Remove(selected);
                ProcessesGrid.Items.Refresh();
                MessageBox.Show("Процес успішно завершено");
            }
            else
            {
                MessageBox.Show("Недостатньо прав для завершення процеса");
            }
        }
    }
}
