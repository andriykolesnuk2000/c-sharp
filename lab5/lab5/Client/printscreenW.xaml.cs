﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Логика взаимодействия для printscreenW.xaml
    /// </summary>
    public partial class printscreenW : Window
    {
        Bitmap printscreen;

        public printscreenW(Bitmap _printscreen)
        {
            InitializeComponent();

            printscreen = _printscreen;

            ImageSourceConverter ISC = new ImageSourceConverter();

            ImageForPrintscreen.Source = BitmapToImageSource(_printscreen);
        }

        BitmapImage BitmapToImageSource(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }

        private void bSave_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "(*.jpg)|*.jpg";
            saveFileDialog.ShowDialog();
            try
            {
                if (saveFileDialog.FileName != null)
                {
                    printscreen.Save(saveFileDialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                MessageBox.Show("Скриншот успешно сохранен\n" + saveFileDialog.FileName);
            }
            catch
            {
                MessageBox.Show("Ошибка при сохранении");
            }
        }
    }
}
