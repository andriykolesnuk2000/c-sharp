﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.ServiceModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace Library
{
    public class Proc
    {

        public string Name { get; set; }
        public long Memory { get; set; }
        public DateTime Time { get; set; }
        public string Priority { get; set; }
        public int CountThreads { get; set; }
        public int Id { get; set; }

        public Proc()
        {
        }

        public Proc(string name, long memory, DateTime time, string priority, int countThreads, int id)
        {
            Name = name;
            Memory = memory;
            Time = time;
            Priority = priority;
            CountThreads = countThreads;
            Id = id;
        }
    }

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class Net : INet
    {
        public List<Proc> GetProcesses()
        {
            List<Proc> processes = new List<Proc>();
            foreach (var proc in Process.GetProcesses())
                try
                {
                    processes.Add(new Proc(proc.ProcessName, proc.VirtualMemorySize64, proc.StartTime, proc.PriorityClass.ToString(), proc.Threads.Count, proc.Id));
                }
                catch { }
            return processes;
        }

        public Bitmap GetScreen()
        {
            Bitmap printscreen = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);

            

            Graphics graphics = Graphics.FromImage(printscreen as Image);

            graphics.CopyFromScreen(0, 0, 0, 0, printscreen.Size);

            return printscreen;
        }

        public bool KillProcessById(int id)
        {
            try
            {
                Process.GetProcessById(id).Kill();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void ShowMessage(string message)
        {
            System.Windows.MessageBox.Show(message);
        }

        public void StartProgram(string name)
        {
            if (name.EndsWith(".exe"))
            {
                Process.Start(name);
            }
            else
            {
                Process.Start(name + ".exe");
            }
        }
    }
}
