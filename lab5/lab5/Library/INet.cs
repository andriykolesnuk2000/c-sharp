﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Drawing;

namespace Library
{


    [ServiceContract]
    public interface INet
    {
        [OperationContract(IsOneWay = true)]
        void ShowMessage(string message);

        [OperationContract]
        List<Proc> GetProcesses();

        [OperationContract]
        bool KillProcessById(int id);

        [OperationContract(IsOneWay = true)]
        void StartProgram(string name);

        [OperationContract]
        Bitmap GetScreen();
    }
}
