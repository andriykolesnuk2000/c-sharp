﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Discovery;
using System.Diagnostics;
using Library;

namespace Server
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ServiceHost service;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            string name = Dns.GetHostName();
            var addresses = Dns.GetHostAddresses(name);
            foreach (var elem in addresses)
            {
                if (elem.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {

                    string baseAddress = $"http://{elem.ToString()}:9000/SOA/";
                    //Console.WriteLine(baseAddress);
                    Type contract = typeof(INet);
                    Type implementation = typeof(Net);
                    string address = baseAddress + implementation.Name;
                    var binding = new BasicHttpBinding();
                    service = new ServiceHost(implementation,
                        new Uri[] { new Uri(address) });
                    service.AddServiceEndpoint(contract, binding, address);
                    var sb = new ServiceMetadataBehavior();
                    sb.HttpGetEnabled = true;
                    service.Description.Behaviors.Add(sb);
                    service.AddServiceEndpoint(typeof(IMetadataExchange),
                        MetadataExchangeBindings.CreateMexHttpBinding(),
                        "mex");

                    service.AddServiceEndpoint(new UdpDiscoveryEndpoint());
                    service.Description.Behaviors.Add(new ServiceDiscoveryBehavior());

                    service.Open();
                }

            }
        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            service.Close();
        }
    }
}
