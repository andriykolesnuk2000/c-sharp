﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ServiceModel;
using System.Net;

namespace Server
{
    [ServiceContract]
    public interface IMyService
    {
        // Далее идут 2 метода, которые будем запрашивать у службы
        // Просто опишем их, реализовывать будем в классе
        // Сложение
        [OperationContract]
        double GetSum(double i, double j);
        // Умножение
        [OperationContract]
        double GetMult(double i, double j);

        [OperationContract]
        void PrintMessage(string str);
    }
    // Реализация методов, которые описаны в интерфейсе
    public class MyService : IMyService
    {
        public double GetSum(double i, double j)
        {
            return i + j;
        }

        public double GetMult(double i, double j)
        {
            return i * j;
        }

        public void PrintMessage(string str)
        {
            MessageBox.Show(str);
        }
    }

    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ServiceHost host;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void StartServerButton_Click(object sender, RoutedEventArgs e)
        {

            ServiceHost host = new ServiceHost(typeof(MyService), new Uri($"http://localhost:9000/MyService/"));
            // Добавляем конечную точку службы с заданным интерфейсом, привязкой (создаём новую) и адресом конечной точки
            host.AddServiceEndpoint(typeof(IMyService), new BasicHttpBinding(), "");
            // Запускаем службу
            host.Open();
        }

        private void StopServerButton_Click(object sender, RoutedEventArgs e)
        {            
            // Закрываем службу
            host.Close();
        }
    }
}
