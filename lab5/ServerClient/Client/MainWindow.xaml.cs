﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ServiceModel;
using System.Net;

namespace Client
{
    [ServiceContract]
    public interface IMyService
    {
        // Сложение
        [OperationContract]
        double GetSum(double i, double j);
        // Умножение
        [OperationContract]
        double GetMult(double i, double j);

        [OperationContract]
        void PrintMessage(string str);
    }

    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        IMyService service;
        public MainWindow()
        {
            InitializeComponent();

            string name = Dns.GetHostName();
            var addresses = Dns.GetHostAddresses(name);

            Uri tcpUri = new Uri($"http://localhost:9000/MyService/");
            // Создаём сетевой адрес, с которым клиент будет взаимодействовать
            EndpointAddress address = new EndpointAddress(tcpUri);
            BasicHttpBinding binding = new BasicHttpBinding();
            // Данный класс используется клиентами для отправки сообщений
            ChannelFactory<IMyService> factory = new ChannelFactory<IMyService>(binding, address);
            // Открываем канал для общения клиента с со службой
            service = factory.CreateChannel();
        }

        private void SendMessage_Click(object sender, RoutedEventArgs e)
        {


            service.PrintMessage(Message.Text);

           //MessageBox.Show(service.GetSum(5, 5).ToString());
        }
    }
}
