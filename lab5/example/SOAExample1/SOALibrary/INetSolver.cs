﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace SOALibrary
{
    [ServiceContract]
    public interface INetSolver
    {
        [OperationContract]
        void ShowMessage(string message);
        [OperationContract]
        void Exit();
        [OperationContract]
        double SolveTask(double x, double y);
    }
}
