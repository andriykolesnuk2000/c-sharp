﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOALibrary
{
    public class NetSolver : INetSolver
    {
        public void Exit()
        {
            Environment.Exit(0);
        }

        public void ShowMessage(string message)
        {
            Console.WriteLine(message);
        }

        public double SolveTask(double x, double y)
        {
            double res = Math.Sin(x) + Math.Pow(x, y) * 2;
            return res;
        }
    }
}
