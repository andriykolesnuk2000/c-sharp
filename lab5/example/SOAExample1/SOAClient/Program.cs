﻿using SOAClient.ServiceReference1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace SOAClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var binding = new BasicHttpBinding();
            var address = "http://10.10.225.34:9000/SOA/NetSolver";
            NetSolverClient client
                = new NetSolverClient(binding, 
                new EndpointAddress(address));
            client.ShowMessage("Hello!");
            double res = client.SolveTask(10, 20);
        
            

        }
    }
}
