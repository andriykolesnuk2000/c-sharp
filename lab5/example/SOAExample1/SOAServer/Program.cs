﻿using SOALibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace SOAServer
{
    class Program
    {
        static void Main(string[] args)
        {
            string baseAddress = "http://10.10.225.34:9000/SOA/";
            Type contract = typeof(INetSolver);
            Type implementation = typeof(NetSolver);
            string address = baseAddress + implementation.Name;
            var binding = new BasicHttpBinding();
            var service = new ServiceHost(implementation, 
                new Uri[] { new Uri(address) });
            service.AddServiceEndpoint(contract, binding, address);
            var sb = new ServiceMetadataBehavior();
            sb.HttpGetEnabled = true;
            service.Description.Behaviors.Add(sb);
            service.AddServiceEndpoint(typeof(IMetadataExchange),
                MetadataExchangeBindings.CreateMexHttpBinding(),
                "mex");

            service.Open();
            Console.ReadLine();

        }
    }
}
