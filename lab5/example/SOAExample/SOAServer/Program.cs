﻿using SOALibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Discovery;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace SOAServer
{
    class Program
    {
        static void Main(string[] args)
        {
            string name = Dns.GetHostName();
            var addresses = Dns.GetHostAddresses(name);
            foreach (var elem in addresses)
            {
                if (elem.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {

                    string baseAddress = $"http://{elem.ToString()}:9000/SOA/";
                    Console.WriteLine(baseAddress);
                    Type contract = typeof(INetSolver);
                    Type implementation = typeof(NetSolver);
                    string address = baseAddress + implementation.Name;
                    var binding = new BasicHttpBinding();
                    var service = new ServiceHost(implementation,
                        new Uri[] { new Uri(address) });
                    service.AddServiceEndpoint(contract, binding, address);
                    var sb = new ServiceMetadataBehavior();
                    sb.HttpGetEnabled = true;
                    service.Description.Behaviors.Add(sb);
                    service.AddServiceEndpoint(typeof(IMetadataExchange),
                        MetadataExchangeBindings.CreateMexHttpBinding(),
                        "mex");

                    service.AddServiceEndpoint(new UdpDiscoveryEndpoint());
                    service.Description.Behaviors.Add(new ServiceDiscoveryBehavior());

                    service.Open();
                }
               
            }
            Console.ReadLine();
        }
    }
}
