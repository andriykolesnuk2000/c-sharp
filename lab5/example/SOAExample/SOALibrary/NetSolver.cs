﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Windows.Forms;

namespace SOALibrary
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class NetSolver : INetSolver
    {
        protected List<string> Messages;
        public NetSolver()
        {
            Messages = new List<string>();
        }

        public void AddMessage(string mess)
        {
            Messages.Add(mess);
            Console.WriteLine(mess);
        }

        public void Exit()
        {
            Environment.Exit(0);
        }

        public List<string> GetMessages()
        {
            return Messages;
        }

        public void Message(string mess)
        {
            Console.WriteLine(mess);
        }

        public void ShowMessage(string message)
        {
            Console.WriteLine(message);
        }

        public double SolveTask(double x, double y)
        {
            double res = Math.Sin(x) + Math.Pow(x, y) * 2;
            return res;
        }
    }
}
