﻿using SOAClient.ServiceReference1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel.Discovery;

namespace SOAClient
{
    class Program
    {
        static void Main(string[] args)
        {
            UdpDiscoveryEndpoint endpoint = new UdpDiscoveryEndpoint();
            DiscoveryClient dc = new DiscoveryClient(endpoint);
            FindCriteria criteria = new FindCriteria(typeof(INetSolver));
            criteria.Duration = new TimeSpan(0, 0, 1);
            FindResponse response = dc.Find(criteria);
            string address = "";
            for (int i = 0; i < response.Endpoints.Count; i++)
                address = response.Endpoints[i].Address.ToString();

            var binding = new BasicHttpBinding();
            //NetSolverClient client
            //    = new NetSolverClient(binding, 
            //    new EndpointAddress(address));
            Console.WriteLine(address);

            ChannelFactory<INetSolver> factory = new ChannelFactory<INetSolver>(binding, address);
            // Открываем канал для общения клиента с со службой
            INetSolver service = factory.CreateChannel();

            service.AddMessage("Test");
            service.AddMessage("Hello");
            List<string> messages = service.GetMessages().ToList<string>();
            Console.WriteLine($"Count : {messages.Count}");
            foreach (var elem in messages)
                Console.WriteLine(elem);

            Console.ReadLine();
        }
    }
}
